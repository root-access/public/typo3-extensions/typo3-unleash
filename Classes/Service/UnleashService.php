<?php
declare(strict_types=1);

namespace RootAccess\Unleash\Service;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

use Unleash\Client\Unleash;
use Unleash\Client\Configuration\Context as UnleashContext;
use Unleash\Client\DTO\Variant as UnleashVariant;

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

use RootAccess\Unleash\Context\UnleashAspect;

class UnleashService implements LoggerAwareInterface, SingletonInterface {
	use LoggerAwareTrait;

	private Context $context;
	private ?Unleash $unleash = null;

	public function __construct (
		Context $context
	) {
		$this->context = $context;
	}

	public function getUnleash (): ?Unleash {
		if (! $this->unleash instanceof Unleash) {
			try {
				/**
				 * There is no aspect "unleash" configured so we can't figure out the exact type to return when calling
				 * TYPO3\CMS\Core\Context\Context::getAspect
				 * 💡 You should add custom aspects to the typo3.contextApiGetAspectMapping setting.
				 * @phpstan-ignore-next-line
				 */
				$unleashAspect = $this->context->getAspect('unleash');
				if ($unleashAspect instanceof UnleashAspect) {
					$this->unleash = $unleashAspect->getUnleash();
				}
			}
			catch (AspectNotFoundException $ex) {
				$this->getLogger()->error(sprintf('%s :: Could not find Unleash instance in Context!', __FUNCTION__), [
					'exception' => $ex,
				]);
			}
		}
		return $this->unleash;
	}

	public function isEnabled (
		string $featureName,
		bool $default = false,
		?UnleashContext $context = null
	): bool {
		if ($this->getUnleash() instanceof Unleash) {
			return $this->getUnleash()->isEnabled($featureName, $context, $default);
		}
		$this->getLogger()->error(sprintf('%s :: Unable to find Unleash instance! Returning default value!', __FUNCTION__), [
			'featureName' => $featureName,
			'default' => $default,
			'context' => $context,
		]);
		return $default;
	}

	public function getVariant (
		string $featureName,
		?UnleashContext $context = null,
		?UnleashVariant $fallbackVariant = null
	): UnleashVariant {
		if ($this->getUnleash() instanceof Unleash) {
			return $this->getUnleash()->getVariant($featureName, $context, $fallbackVariant);
		}
		throw new \RuntimeException(
			'Unable to find Unleash instance!',
			1656006000
		);
	}

	protected function getLogger (): Logger {
		if (! $this->logger instanceof Logger) {
			$this->logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(static::class);
		}
		return $this->logger;
	}

	public static function getInstance (): self {
		return GeneralUtility::makeInstance(static::class);
	}
}
