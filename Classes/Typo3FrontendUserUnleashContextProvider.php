<?php
declare(strict_types=1);

namespace RootAccess\Unleash;

use Psr\Http\Message\ServerRequestInterface;

use Unleash\Client\ContextProvider\UnleashContextProvider;
use Unleash\Client\Configuration\UnleashContext;
use Unleash\Client\UnleashBuilder;

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Core\ApplicationContext;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;

final class Typo3FrontendUserUnleashContextProvider implements UnleashContextProvider {

	private UnleashContext $unleashContext;

	private function __construct (
		ServerRequestInterface $request,
		Context $context
	) {

		$normalizedParams = $request->getAttribute('normalizedParams');
		$site = $request->getAttribute('site');
		$pageArguments = $request->getAttribute('routing');
		$frontendUser = $request->getAttribute('frontend.user');

		$frotnendUserAspect = $context->getAspect('frontend.user');

		$currentUserId = null; // ?string
		$ipAddress = $normalizedParams->getRemoteAddress();
		$sessionId = null; // ?string
		$customContext = []; // array
		$hostname = $normalizedParams->getHttpHost();
		$environment = null; // ?string
		$currentTime = $context->getAspect('date')->getDateTime()->setTimezone(
			new \DateTimeZone(ini_get('date.timezone') ?: 'Europe/Berlin')
		);

		if ($frotnendUserAspect->isLoggedIn()) {
			/** @var int $currentUserId */
			$currentUserId = $frotnendUserAspect->get('id');
			/** @var string $username */
			$username = $frotnendUserAspect->get('username');
			$groupIds = implode(',', $frotnendUserAspect->getGroupIds());
			$groupNames = implode(',', $frotnendUserAspect->getGroupNames());

			$currentUserId = (string)$currentUserId;
			$customContext['username'] = $username;
			$customContext['groupIds'] = $groupIds;
			$customContext['groupNames'] = $groupNames;

			$sessionId = $frontendUser->getSession()->getIdentifier();
		}

		$applicationContext = Environment::getContext();
		if ($applicationContext->isProduction()) {
			$environment = 'production';
		}
		if ($applicationContext->isDevelopment()) {
			$environment = 'development';
		}
		if ($applicationContext->isTesting()) {
			$environment = 'testing';
		}

		$this->unleashContext = new UnleashContext(
			$currentUserId,
			$ipAddress,
			$sessionId,
			$customContext,
			$hostname,
			$environment,
			$currentTime
		);
	}

	public function getContext (): UnleashContext {
		return $this->unleashContext;
	}

	public static function create (
		ServerRequestInterface $request,
		Context $context = null
	): self {
		return new self(
			$request,
			$context ?? GeneralUtility::makeInstance(Context::class)
		);
	}
}
