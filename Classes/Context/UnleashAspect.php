<?php
declare(strict_types=1);

namespace RootAccess\Unleash\Context;

use Unleash\Client\Unleash;
use Unleash\Client\Configuration\Context as UnleashContext;
use Unleash\Client\DTO\Variant as UnleashVariant;

use TYPO3\CMS\Core\Context\AspectInterface;
use TYPO3\CMS\Core\Context\Exception\AspectPropertyNotFoundException;

class UnleashAspect implements AspectInterface {

	protected Unleash $unleash;
	protected UnleashContext $unleashContext;

	function __construct (
		Unleash $unleash,
		UnleashContext $unleashContext
	) {
		$this->unleash = $unleash;
		$this->unleashContext = $unleashContext;
	}

	public function getUnleash (): Unleash {
		return $this->unleash;
	}
	public function getUnleashContext (): UnleashContext {
		return $this->unleashContext;
	}

	/**
	 * Get a property from an aspect
	 *
	 * @param string $name
	 * @return mixed
	 * @throws AspectPropertyNotFoundException
	 */
	public function get (string $name) {
		switch($name) {
			case 'instance':
			case 'unleash':
				return $this->getUnleash();
			case 'context':
			case 'unleashContext':
				return $this->getUnleashContext();
		}
		throw new AspectPropertyNotFoundException(
			sprintf('Property "%s" not found in Aspect "%s".', $name, static::class),
			1654073836
		);
	}
}
