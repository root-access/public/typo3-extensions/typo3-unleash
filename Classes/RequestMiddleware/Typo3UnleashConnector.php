<?php
declare(strict_types=1);

namespace RootAccess\Unleash\RequestMiddleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

use Unleash\Client\ContextProvider\UnleashContextProvider;
use Unleash\Client\Unleash;
use Unleash\Client\UnleashBuilder;
use Unleash\Client\Helper\Url;
use Unleash\Client\Configuration\UnleashContext;

use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Context\Context;

use RootAccess\Unleash\Typo3FrontendUserUnleashContextProvider;
use RootAccess\Unleash\Context\UnleashAspect;

class Typo3UnleashConnector implements MiddlewareInterface {

	private Context $context;
	private ExtensionConfiguration $extensionConfiguration;

	/**
	 * @param Context $context
	 * @param ExtensionConfiguration $extensionConfiguration
	 */
	public function __construct (
		Context $context,
		ExtensionConfiguration $extensionConfiguration
	) {
		$this->context = $context;
		$this->extensionConfiguration = $extensionConfiguration;
	}

	/**
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 *
	 * @throws \RuntimeException
	 */
	public function process (
		ServerRequestInterface $request,
		RequestHandlerInterface $handler
	): ResponseInterface {

		$contextProvider = Typo3FrontendUserUnleashContextProvider::create($request);
		$unleash = $this->buildUnleash($contextProvider);
		$unleashContext = $contextProvider->getContext();

		$request = $request->withAttribute('unleash', $unleash);
		$request = $request->withAttribute('unleash.context', $unleashContext);
		$GLOBALS['TYPO3_REQUEST'] = $request;

		$this->context->setAspect('unleash', new UnleashAspect($unleash, $unleashContext));

		return $handler->handle($request);
	}

	/**
	 * @param UnleashContextProvider|null $contextProvider
	 * @return Unleash
	 *
	 * @throws \RuntimeException
	 */
	private function buildUnleash (
		UnleashContextProvider $contextProvider = null
	): Unleash {
		$appName = $this->getValueFromExtConf('appName', true);
		$appUrl = $this->getValueFromExtConf('appUrl', true);
		$apiToken = $this->getValueFromExtConf('apiToken');
		$instanceId = $this->getValueFromExtConf('instanceId');

		$unleash = UnleashBuilder::create()
			->withAppName($appName)
			->withAppUrl($appUrl)
			->withContextProvider($contextProvider)
		;

		if (is_string($instanceId)) {
			$unleash = $unleash->withInstanceId($instanceId);
		}
		if (is_string($apiToken)) {
			$unleash = $unleash->withHeader('Authorization', $apiToken);
		}

		return $unleash->build();
	}

	/**
	 * @param string $path
	 * @param bool $isRequired
	 * @param mixed $default
	 * @return mixed
	 *
	 * @throws \RuntimeException
	 */
	private function getValueFromExtConf(
		string $path = '',
		bool $isRequired = false,
		$default = null
	) {

		try {
			return $this->extensionConfiguration->get('unleash', $path);
		}
		catch (
			ExtensionConfigurationExtensionNotConfiguredException
			| ExtensionConfigurationPathDoesNotExistException $ex
		) {
			if ($isRequired) {
				throw new \RuntimeException(
					sprintf('The request valuePath %s does not exists!', $path),
					1654067656,
					$ex
				);
			}

			return $default;
		}
	}
}
