<?php
declare(strict_types=1);

use RootAccess\Unleash\RequestMiddleware\Typo3UnleashConnector;

return [
	'frontend' => [
		'root-access/unleash/unleash-connector' => [
			'target' => Typo3UnleashConnector::class,
			'after' => [
				'typo3/cms-frontend/tsfe',
			],
			'before' => [
				'typo3/cms-frontend/prepare-tsfe-rendering',
			],
		],
	]
];
