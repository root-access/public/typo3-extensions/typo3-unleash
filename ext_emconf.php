<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'Simple TYPO3 CMS Extension to use Unleash',
	'description' => 'This TYPO3 CMS Extension provides a simple interface to communicate with a Unleash server to check for feature toggles. IMPORTANT: This extension is intended to be installed via composer, due to requirements of the unleash/client package!',
	'category' => 'misc',
	'state' => 'stable',
	'clearCacheOnLoad' => true,
	'version' => '1.0.0',
	'constraints' => [
		'depends' => [
			'typo3' => '10.4.0-11.5.99',
		],
		'conflicts' => [
		],
		'suggests' => [
		],
	],
];
